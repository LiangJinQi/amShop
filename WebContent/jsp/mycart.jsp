<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<script src="/cart/js/jquery-3.2.1.js" type="text/javascript" charset="utf-8"></script>
<link rel="stylesheet" type="text/css" href="/cart/css/mycart.css"/>
<script type="text/javascript"> 
$(function(){
     
	//统计商品数量
	selectNumber();
	//计算商品小计
	xiaoji();
	//计算总计
	showtotal();
	//计算节省
	savaMoney();
	/*
	 * 、判断是是否全选
	 *  全选框点击事件
	 */	
	$("#allchecked").click(function(){
		
		var bool =document.getElementById("allchecked").checked;
		if(bool)
		{
			//统计选择的商品数量
			var count = $(":checkbox[name=checkedbtn]").length;
			$("#selectnumber").text(count+"件商品");
			setchecked(true);
		    setoreder(true);
		}
		else if(!bool)
		{
			//统计选择的商品数量
			$("#selectnumber").text(0+"件商品");
			setchecked(false);
			setoreder(false);
		}
		//计算总计
		showtotal();
		//计算节省
		savaMoney();
	});
	/*
     * 判断个选和全选
     * 商品条目点击事件
    */
   
   $(":checkbox[name=checkedbtn]").click(function(){
   	    var a =$(":checkbox[name=checkedbtn]").length;//得到所有复选框的数目
   	    var b= $(":checkbox[name=checkedbtn]");//获取所有复选框的元素
   	    //统计商品数量
   	    selectNumber();
   	    var c=0;//勾选状态中商品数量
   	    for(var i=0;i<a;i++)
   	    {
   	    	if(b[i].checked==true)
   	    	{ 
   	    		c++;
   	    	}	
   	    }
   	   if(c==a)
   	   {
   	   	  document.getElementById("allchecked").checked=true;
   	   	   setoreder(true);
   	   }
   	   else if(c==0)
   	   {
   	   	 document.getElementById("allchecked").checked=false;
   	   	 setoreder(false);
   	   }
   	   else
   	   {
   	    document.getElementById("allchecked").checked=false;
   	   	setoreder(true);
   	   }
   	   //计算总计
   	 showtotal();
   //计算节省
 	savaMoney();
   });
   /*******商品数量按钮*********/
   /*
    * 减
    */
   $(".jian").click(function(){
   	   //获取商品数量
	   var id=$(this).attr("id").substring(0,1);
   	   var shopping_number=$("#"+id+"shoppingnumber").val();
   	   //调用方法
   	   if(shopping_number==1)
   		   {
   		      if(confirm("您是否要删除此商品？"))
   		    	  {
   		    	  location = "/cart/CartItemServlet?method=batchdelete&cart_ids="+id;
   		    	  }
   		   }
   	   else{
   		shopping_number=--shopping_number;
   	   sendupdataquantity(id,shopping_number);
   	   }
   	   $("#shoppingnumber").attr("value",shopping_number); 
   	   //获取商品单价
   	   var shopping_price=$("#shifu_price").text();
   	   var xiao_total=parseInt(shopping_price)*shopping_number;
    	showtotal();
    	//计算节省
    	savaMoney();
   	   
   });
   /*
    * 加
    */
    $(".jia").click(function(){
   	   //获取商品数量
       var id=$(this).attr("id").substring(0,1);
       var shopping_number=$("#"+id+"shoppingnumber").val();
       shopping_number=++shopping_number;
   	   //调用方法
       sendupdataquantity(id,shopping_number);
   	   $("#shoppingnumber").attr("value",shopping_number);
   	   var shopping_price=$("#shifu_price").text();
   	   var xiao_total=parseInt(shopping_price)*shopping_number;
       showtotal();
        //计算节省
    	savaMoney();
   });
  
});

/*
* 添加单个商品勾选
*/
function setchecked(bool)
{
var L = $(":checkbox[name=checkedbtn]").length;
var cx =$(":checkbox[name=checkedbtn]");
for(var i=0;i<L;i++)
{
	cx[i].checked=bool;
}
}
/*
* 结算样式
*/
 function setoreder(bool)
   {
if(bool)
{
	$("#submitorder").removeClass("killorder").addClass("submitorder");
	$("#submitorder").unbind("click");//恢复所有click事件
}
 else
   {
	 $("#submitorder").removeClass("submitorder").addClass("killorder");
	 $("#submitorder").click(function(){   
		return false;
	});//去除所有点击事件
	
   }
 }
 /*
  * 计算总计
  */
 function showtotal()
 {
 	var total=0;
 	/*
 	 * 获取所有被勾选的复选框再循环遍历
 	 */
 	$(":checkbox[name=checkedbtn]:checked").each(function(){
 		var id=$(this).val();
 		total+=Number($("#"+id+"xiaoji").text());
 	});
 	$("#total_money").text(total);
 }
 //计算节省
 function savaMoney()
 {
	 var total=0;
	 var sctotal=0;
	 	/*
	 	 * 获取所有被勾选的复选框再循环遍历
	 	 */
	 	$(":checkbox[name=checkedbtn]:checked").each(function(){
	 		var id=$(this).val();
	 		var scprice=Number($("#"+id+"shichang_price").text());
	 		var quantity=Number($("#"+id+"shoppingnumber").val());
	 		sctotal+=scprice*quantity;
	 		total+=Number($("#"+id+"xiaoji").text());
	 	});
	 	$("#save_money").text(sctotal-total);
 }
 //统计购物商品的数量
 function selectNumber()
 {
	var count = $(":checkbox[name=checkedbtn]:checked").length;
	$("#selectnumber").text(count+"件商品");
 }
 //计算小计
  function xiaoji()
 {   
	  /*
	 	 * 获取所有商品
	 	 */
	 	$(":checkbox[name=checkedbtn]").each(function(){
	 		var id=$(this).val();
	 		var price=Number($("#"+id+"shifu_price").text());
	 	      var quantity=Number($("#"+id+"shoppingnumber").val());
	 	    	$("#" + id + "xiaoji").text(quantity*price);
	 	});
 }
 //批量删除
 function batchdelete()
 {
	 //创建数组
	 var cartidsArray= new Array();
		$(":checkbox[name=checkedbtn]:checked").each(function(){
	 		cartidsArray.push($(this).val());
	 	});
		 location = "/cart/CartItemServlet?method=batchdelete&cart_ids="+cartidsArray;
 }
 //提交被勾选的商品
 function payMoney()
 {
	 //创建数组
	 var cartidsArray= new Array();
		$(":checkbox[name=checkedbtn]:checked").each(function(){
	 		cartidsArray.push($(this).val());
	 	});
		 location = "/cart/CartItemServlet?method=submitorder&cart_ids="+cartidsArray;
 }
//请求服务器，修改数量
function sendupdataquantity(id,quantity)
{ 
	 $.ajax({  
	        type: "POST",  
	        url: "/cart/CartItemServlet", //orderModifyStatus  
	        data: {"method":"updateQuantity","cart_id":id,"cart_quantity":quantity},  
	        dataType:"json",  
	        async:false,  //同步请求
	        cache:false,  //清楚缓存
	        success:function(result) {
				//1. 修改数量
				$("#" + id + "shoppingnumber").val(result);
				//2. 修改小计
				 var price=Number($("#"+id+"shifu_price").text());
	           	 var rs=Number(result);
	           	$("#" + id + "xiaoji").text(rs*price);
	            //3. 重新计算总计
				showTotal();
				//计算节省
				savaMoney();    	
			}
	    });  
}
</script>
</head>
<body>
    <div class="content">
			  <div class="content_heard">
			  	   <div class="content_heard_left">
			  	   </div>
			  	   <div class="content_heard_right">
			  	   </div>
			  </div>
  <c:choose>
  <c:when test="${empty cart }">
		<div class="emptycart">
			  <table border="0px" cellspacing="0" cellpadding="0">
			  	<tr>
			  		<td style="width: 200px; height: 150px;">
			  			<img src="/cart/img/empty_cart.png"/>
			  		</td>
			  		<td style="width: 1050px; text-align: center;">
			  			<p style="font-size: 40px;">您的购物车是空的</p>
			  		</td>
			  	</tr>
			  </table>
		</div>
 </c:when>
 <c:otherwise>
		<div class="content_table" >
			
			  <table border="1px" cellspacing="0" cellpadding="0" id="tb">
			  	<tr style="background-color:lightgray">
			  		<td colspan="3">商品</td>
			  		<td>市场价(元)</td>
			  		<td>购买价(元)</td>
			  		<td>数量(个)</td>
			  		<td>小计(元)</td>
			  		<td>操作</td>
			  	</tr>
			  	<!--
                  	作者：1140086359@qq.com
                  	时间：2017-12-11
                  	描述：商品信息
                  -->
			  <c:forEach items="${cart }" var="l">
			  	<tr>
			  		<td style="width: 50px;"><input type="checkbox" name="checkedbtn" id="checked" value="${l.id }" checked="true"  /></td>
			  		<td style="text-align: center;" height="100px" width="100px"><img src="../img/taobao_heard.png"/></td>
			  		<td style="width: 600px;">
			  			<a href="">asdasdsadqweqweqwead</a>
			  			<br/>
			  			<p>${l.shoppinginformation }</p>
			  		</td>
			  		<td style="width: 150px;"><span id="${l.id }shichang_price">${l.shoppingscprice }</span></td>
			  		<td style="width: 150px;"><span id="${l.id }shifu_price">${l.shoppingsjprice }</span></td>
			  		<td style="width: 150px;">
			  			<a id="${l.id }jian" class="jian">-</a>
			  			<input type="text" id="${l.id }shoppingnumber" class="shoppingnumber" value="${l.quantity }" />
			  			<a id="${l.id }jia" class="jia">+</a>
			  		</td>
			  		<td style="width: 150px;"><span id="${l.id }xiaoji"></span></td>
			  		<td style="width: 50px;"><a href="<c:url value='/CartItemServlet?method=batchdelete&cart_ids=${l.id }'/>">删除</a></td>
			  	</tr>
			  	</c:forEach>
			  </table>
			  <!--
              	作者：1140086359@qq.com
              	时间：2017-12-11
              	描述：结算表
            -->
			 <table border="1px" cellspacing="1" cellpadding="1" id="tb2">
			 	<tr>
			 		<td style="width: 800px; height: 50px;" colspan="3">
			 			<div>
			 				<ul id="tb2_ul">
			 					<li style="width: 30px;"><input type="checkbox" name="allchecked" id="allchecked"   /></li>
			 					<li><p style="color: red;">全选</p></li>
			 					<li><a href="javascript:batchdelete();">批量删除</a></li>
			 				</ul>
			 			</div>
			 		</td>
			 		<td style="width: 200px;"><p>已选择:</p></td>
			 		<td style="width: 200px;"><p id="selectnumber"></p></td>
			 	</tr>
			 	<tr>
			 		
			 		<td style="width: 800px; height: 50px;" colspan="3">   </td>   
			 		<td style="width: 200px;"><span>共节省:</span></td>
			 		<td style="width: 200px;"><span id="save_money"></span></td>
			 	</tr>
			 	<tr>
			 		<td style="width: 800px; height: 50px;" colspan="3">   </td>   
			 		<td style="width: 200px;">总计:</td>
			 		<td style="width: 200px;"><span id="total_money"></span></td>
			 	</tr>
			 </table>
			 
		</div>
		  <div class="content_button">
		  	<div id="content_button_ul">
		  	  <ul>
		  	  	<li>
		  	  		<a href="#" id="goshoppinp" class="goshoppinp">
		  	  		继续购物
		  	  		</a>
		  	  	</li>
		  	  	<li><a href="javascript:payMoney()" id="submitorder" class="submitorder" >去结算</a></li>
		  	  </ul>
		  	</div>
		  </div>
	</c:otherwise>
 </c:choose>
		  <div class="content_tuijian_heard">
		  	<span>为您推荐</span>
		  </div>
		  <div class="content_tuijian">
		  </div>
    </div>
</body>
</html>