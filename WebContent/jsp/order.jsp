<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>订单</title>
<script src="/cart/js/jquery-3.2.1.js" type="text/javascript" charset="utf-8"></script>
<link rel="stylesheet" type="text/css" href="/cart/css/order.css"/>
<script src="/cart/js/geo.js" type="text/javascript" charset="utf-8"></script>
<script type="text/javascript">
$(function(){
	setup();
 	preselect();
 	//计算小计；
 	xiaoji();
 	//计算总计
 	totalMoney();
    //留言字数检查
    leaveWords();
 	
     var zhezhao=document.getElementById("zhezhao"); 
     var login=document.getElementById("login"); 
     var bt=document.getElementById("bt"); 
     var btclose=document.getElementById("btclose"); 

  bt.onclick=function() 
    { 
        zhezhao.style.display="block"; 
        login.style.display="block";  
    } 
  btclose.onclick=function() 
   { 
        zhezhao.style.display="none"; 
        login.style.display="none";  
   } 

 	//选择地址
 	$("#baocun").click(function(){
 		ghAddress();
 	});
 });
  //跟换地址
  function ghAddress()
  {
         var s1 = $("#s1").val();
         var s2 =  $("#s2").val();
         var s3 =  $("#s3").val();
         //跟换收货人姓名
         var name= $("#username").val();
         //跟换电话号码
         var telnumber =$("#telnumber").val();
         //获取收获人姓名
   	  //手机正则表达式
   	  var shouji=/^1[3|4|5|8][0-9]\d{4,8}$/;
   	  if(s1==""||s2==""||s3==""||name==""||telnumber=="")
   		  {
   		     alert("请将地址信息填加完整!");
   		  }
   	  else if(!shouji.test(telnumber))
   		  {
   		   alert("手机号码格式错误!");
   		  }
   	  else
   		  {
   		 $("#shAddress").text(s1+s2+s3);
   		 $("#userName").text(name);
   	     $("#tel").text(telnumber);
   	   document.getElementById("zhezhao").style.display="none"; ; 
       document.getElementById("login").style.display="none"; ; 
   		  }
  }
  //计算小计
  function xiaoji()
  {  
	  /*
	 	 * 获取所有商品
	 	 */
	 	$("span[name=sp]").each(function(){
	 		  var id=$(this).attr("id").substring(0,1);
	 		  var price=Number($("#"+id+"sjprice").text());
	 	      var quantity=Number($("#"+id+"quantity").text());
	 	      $("#" + id + "xiaoji").text(quantity*price);
	 	});
  }
  //计算总计
  function totalMoney()
  {

	  var total=0;
	  /*
	 	 * 获取所有商品小计
	 	 */

	  $("span[name=spxj]").each(function(){
 		  var id=$(this).attr("id").substring(0,1);
 		  var xiaoji= Number($("#"+id+"xiaoji").text());
 		  total+=xiaoji;
 	});
	 $("#pay_totalmoney").text("应付金额:"+total);
  }
  //addOrder增加订单
  function addOrder()
  {
	  //获取收获人姓名
	  var userName=$("#userName").text();
	  //获取收货地址
	  var shAddress=$("#shAddress").text();
	  //获取收货人电话号码
	  var telnumber=$("#tel").text();
	  var total=0;
	  /*
	  * 获取所有商品小计
	 */

	    $("span[name=spxj]").each(function(){
 		  var id=$(this).attr("id").substring(0,1);
 		  var xiaoji= Number($("#"+id+"xiaoji").text());
 		  total+=xiaoji;
 	});
	  
	  //创建数组
	  var shXxArray= new Array();
	  //选择商品ID数组
	   var OrderIds= new Array();
	  shXxArray.push(userName);
	  shXxArray.push(shAddress);
	  shXxArray.push(telnumber);
	  shXxArray.push(total);
	  $("span[name=spxj]").each(function(){
		var id=$(this).attr("id").substring(0,1);
	 	  OrderIds.push(id);
	 	});
	//显示选择的物流信息
	   	selectWuliu();
		//location = "/cart/OrderServlet?method=addOrder&shXxArray="+shXxArray+"&OrderIds="+OrderIds;
  }
  //获取选择快递信息
  function selectWuliu()
  {
	 var wl=$("#wuliu option:selected");
	 alert(wl.val());
	 var leaveWords=$("#leaveWords").val();
	 
  }
  //提交留言
  function leaveWords()
  {
	  $("#leaveWords").keyup(function(){
		    var length=50-Number($("#leaveWords").val().length);
          $("#wslength").text(length);
		});
  }
</script>
</head>
<body>
		<div class="zhezhao" id="zhezhao"></div> 
              <div class="login" id="login">
  	              <button id="btclose" class="btclose">
  		                                    点击关闭
  	              </button>
  	              <br/>
  <div class="sqx_address">
   <span>地址:</span>	
  <select class="select1" name="province" id="s1">
            <option>==所在省份==</option>
    </select>
  <select class="select2" name="city" id="s2">
            <option>==所在地级市==</option>
  </select>
  <select class="select3" name="town" id="s3">
           <option></option>
  </select>
  <input id="address" name="address" type="hidden" value="" />
  <input onclick="alert(promptinfo())" type="submit" value="提交" />
  </div> 
  <div class="xiangxi_address">
  	<div>
  	   <span id="xiangxi_span" >详细地址:</span>
  	</div>   
  	   <textarea class="xiangxi_textaddress"></textarea>
  </div>
  <div class="yzbm">
    <span>邮政编码:</span><input style="height: 20px;" type="text" id="yzbm" value="" />
  </div>
  <div class="username">
  	<span>收货人姓名:</span><input style="height: 20px;" type="text" id="username" value="" />
  </div>
  <div class="telnumber">
  	<span>手机号码:</span><input style="height: 20px;" type="text" id="telnumber" value="" />
  </div>
  <div class="baocun">
  <input  type="button" id="baocun" value="保存" />
  </div>
  </div>  
		
		<div class="content">
			<div class="content_heard">
			  	   <div class="content_heard_left">
			  	   </div>
			  	   <div class="content_heard_right">
			  	   </div>
			</div>
			<div class="content_address">
			  	<div class="content_address_heard">
			  		 <h3 style="font-size: 20px;">收货信息</h3>
			  		 <a id="addAddress" class="addAddress">新增地址</a>
			  		 <a id="selectAddress" class="selectAddress">选择地址</a>
			  	</div>
			  <div class="content_address_body">
			  	<table id="tb1" border="0px">
			  		<tr>
			  	  		<td style="height: 80px;width:50px ;">
			  	  			<img src="../img/address.png"/>
			  	  		</td>
			  	  		<td style="height: 80px;width: 150px;">
			  	  			<span>收货人:</span><span id="userName"></span>
			  	  		</td>
			  	  		<td style="height: 80px;width: 600px;">
			  	  			<span >收获地址:</span><span id="shAddress"></span>
			  	  		</td>
			  	  		<td style="height: 80px; width:200px ;">
			  	  			<span>电话:</span><span id="tel"></span>
			  	  		</td>
			  	  		<td style="height: 80px; width:100px ; text-align: center;">
			  	  			<span>默认地址</span>
			  	  		</td>
			  	  		<td style="height: 80px; width:50px ; text-align: center;">
			  	  			<a id="modify_address" href="#"><input type="button" id="bt" value="修改" /></a>
			  	  		</td>
			  	  		<td style="height: 80px; width:50px ; text-align: center;">
			  	  			<span><a id="delete_address" href="#">删除</a></span>
			  	  		</td>
			  	  		</tr>
			  	</table>
			 </div>
			</div>
			 <div class="content_tb2">
			  	 <table border="1px" id="tb2">
			  	 	<tr>
			  	 		<td style="width: 900px;height: 50px; text-align: center;" colspan="2">商品信息</td>
			  	 	    <td style="width: 100px; text-align: center">单价(元)</td>
			  	 	    <td style="width: 100px; text-align: center">数量（个）</td>
			  	 	    <td style="width: 100px; text-align: center">小计(元)</td>
			  	 	</tr>
			  <c:forEach items="${selectCart }" var="cart">
			  	 	<tr>
			  	 	    <td style="width: 120px;height:100px;"><img src="../img/taobao_heard.png"/></td>
			  	 		<td style="width: 780px;height: 100px;"><span id="${cart.id }shopping_formetion" name="sp">${cart.shoppinginformation }</span></td>
			  	 	    <td style="width: 100px; text-align: center;"><span id="${cart.id }sjprice" name="sp">${cart.shoppingsjprice }</span></td>
			  	 	    <td style="width: 100px; text-align: center;"><span id="${cart.id }quantity" name="sp">${cart.quantity  }</span></td>
			  	 	    <td style="width: 100px; text-align: center;"><span id="${cart.id }xiaoji" name="spxj"></span></td>
			  	 	</tr>
			  </c:forEach>
			  	 	<tr>
			  	 		<td style="height: 50px;" colspan="5">
			  	 		<span id="select_wuliu">选择物流:
			  	 			<select id="wuliu">
			  	 				<option value="申通快递">申通物流</option>
			  	 				<option value="顺丰快递">顺丰物流</option>
			  	 				<option value="韵达快递">韵达物流</option>
			  	 				<option value="中通快递">中通物流</option>
			  	 			</select>
			  	 		</span>
			  	 	</td>
			  	 	</tr>
			  	 <tr>
			  	 	<td style="height: 50px;" colspan="5">
			  	 		<span id="liuyan" >给卖家留言:
			  	 		<input type="text" id="leaveWords" value="" style="width: 800px;"/>
			  	 		<span><span id="wslength">50</span>/50</span>
			  	 		</span>
			  	 	</td>
			  	 </tr>
			  	 </table>
			  	 </div>
			    <div class="content_tb3">
			    	<span id="pay_mima">支付密码
			    		<input style="width: 200px; height: 22px;" type="password" name="" id="" value="" />
			    	</span>
			    	<span id="pay_totalmoney"></span>
			    </div>
			  <div class="orderbtn">
			  	<a id="submitorder" href="javascript:addOrder()">提交订单</a>
			  </div>
		</div>
</body>
</html>