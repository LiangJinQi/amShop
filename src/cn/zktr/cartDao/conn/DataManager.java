package cn.zktr.cartDao.conn;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

public class DataManager {
  
	public DataManager()
	{
		
	}
	/*
	 *  ��ȡ����
	 */
	public Connection getConnection()
	{
		Connection con=null;
		try {
			//��ʼ������
			Context ctx=new InitialContext();
			//��ȡ����
			DataSource ds= (DataSource) ctx.lookup("java:comp/env/jdbc/amcart");
			//��ȡ���Ӷ���
			con=ds.getConnection();
		} catch (NamingException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return con;
	}
	/*
	 * 添删改(无参)
	 */
	public int updateExecuted(String sql)
	{
		return this.updateExecuted(sql, null);
	}
	/*
	 * 添删改(有参)
	 */
	public int updateExecuted(String sql,ParamSet param)
	{
		int row=0;
		PreparedStatement pst=null;
		Connection con=this.getConnection();
		//ִ��sql���
		try {
			 pst=con.prepareStatement(sql);
			 if(param!=null)
			 {
				 //����sql��������
				 param.setimpl(pst);
			 }
			 //ִ��SQL
			 row=pst.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		finally
		{
			this.close(con, pst, null);
		}
		return row;
	}
	/*
	 * 查询单条语句
	 */
	public<T> T query(String sql,RowData<T> row)
	{
		return this.query(sql,null,row);
	}
	
	public<T> T query(String sql,ParamSetimpl param,RowData<T> row)
	{
		List<T> data=this.queryList(sql, param, row);
		if(data!=null && data.size()!=0)
		{
			return data.get(0);
		}
		return null;
	}
	/*
	 * 查询多条语句
	 */
	public<T> List<T> queryList(String sql,RowData<T> row)
	{
		return this.queryList(sql,null,row);
	}
	public<T> List<T> queryList(String sql,ParamSetimpl param,RowData<T> row)
	{
		List<T> lst=new ArrayList<T>();
		Connection con=this.getConnection();
		ResultSet rs=null;
		PreparedStatement pst=null;
		try {
			//ִ��SQL���
			 pst=con.prepareStatement(sql);
			 if(param!=null)
			 {
				 param.setimpl(pst);
			 }
			 rs=pst.executeQuery();
			 while(rs.next())
			 {
				 T t=row.getRowData(rs);
				 lst.add(t);
			 }
		} catch (SQLException e) {
			e.printStackTrace();
		}
		finally
		{
			this.close(con, pst, rs);
		}
		return lst;
	}
	/*
	 * �ر����ݿ�����
	 */
	public void close(Connection con,PreparedStatement pst,ResultSet rs)
	{
		if(rs!=null)
		{
			try {
				rs.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		if(pst!=null)
		{
			try {
				pst.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		if(con!=null)
		{
			try {
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
} 
