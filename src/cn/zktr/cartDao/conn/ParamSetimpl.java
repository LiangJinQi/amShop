package cn.zktr.cartDao.conn;

import java.sql.PreparedStatement;
import java.sql.SQLException;

public interface ParamSetimpl {
  
	/*
	 * 参数设置接口
	 */
	void setimpl(PreparedStatement pst) throws SQLException;
}
