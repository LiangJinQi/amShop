package cn.zktr.cartDao.conn;

import java.sql.PreparedStatement;
import java.sql.SQLException;

public class ParamSet implements ParamSetimpl {
    
	private Object[] param;
	public ParamSet(Object...objects)
	{
		this.param=objects;
	}

	@Override
	public void setimpl(PreparedStatement pst) throws SQLException {
		for(int i=0;i<param.length;i++)
		{
			pst.setObject((i+1), param[i]);
		}
	}
	
}
