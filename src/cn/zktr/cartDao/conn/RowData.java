package cn.zktr.cartDao.conn;

import java.sql.ResultSet;
import java.sql.SQLException;
public interface RowData<T> {
   /*
    * 处理每一条记录
    */
	T getRowData(ResultSet rs) throws SQLException;
}
