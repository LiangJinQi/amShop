package cn.zktr.cartDao.bean;
//创建一个生成订单的类Order

import cn.zktr.cartBean.Order;
import cn.zktr.cartBean.OrderItem;
import cn.zktr.cartDao.conn.DataManager;
import cn.zktr.cartDao.conn.ParamSet;

public class OrderDao {
      //创建DataManager对象
	DataManager dao=new DataManager();
	
	//添加订单addOrder
	public int addOrder(Order order)
	{
		String sql="INSERT INTO `order`(order_uid,order_uname,order_address,order_status,order_time,order_total,order_uTelnumber,order_number,order_wuliu,order_liuyan) " + "VALUES(?,?,?,?,?,?,?,?,?,?)";
		return dao.updateExecuted(sql, new ParamSet(order.getOrder_uid(),order.getOrder_uname(),
				order.getOrder_address(),order.getOrder_status(),order.getOrder_time(),
				order.getTotal(),order.getOrder_uTelnumber(),order.getOrder_number(),order.getOrderItem_wuliu(),
				order.getOrder_leaveWords()));
	}
	//添加订单条目
	public int addOrderItem(OrderItem oitem)
	{
		String sql="INSERT INTO orderitem"+
	"(orderItem_sid,orderItem_uid,orderItem_sname,orderItem_squantity,orderItem_price,orderItem_totalprice,orderItem_imag,order_number) " +
				"VALUES(?,?,?,?,?,?,?,?)";
		return dao.updateExecuted(sql, new ParamSet(oitem.getOrderItem_sid(),oitem.getOrderItem_uid(),oitem.getOrderItem_sname(),
				oitem.getOrderItem_squantity(),oitem.getOrderItem_price(),oitem.getOrderItem_totalprice(),oitem.getOrderItem_image(),oitem.getOrder_number()));
	}
}
