package cn.zktr.cartDao.bean;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import cn.zktr.cartBean.cart;
import cn.zktr.cartDao.conn.DataManager;
import cn.zktr.cartDao.conn.ParamSet;
import cn.zktr.cartDao.conn.RowData;

public class cartdao {
   private RowData<cart> ct; 
   DataManager dao=new DataManager();                         
   public cartdao()
   {
	   ct=new cartData();
   }
	/*
	 * ��ѯ������Ʒ��Ϣ
	 */
   public List<cart> getAllcart()
   {   
	   String sql="select *from cart";
	   return dao.queryList(sql, ct);
   }
   /*
    * 更新购物车中商品数量
    */
   public int update(int id,int quantity)
   {
	   
	   String sql="UPDATE cart SET cart_quantity=? WHERE cart_id=?";
	   return dao.updateExecuted(sql,new ParamSet(quantity,id));
   }
   /*
    * 根据购物车ID删除之指定商品
    */
  public  int batchdelete(String cartIds)
  {
	  //将字符串转为数组
	  Object[] cart_ids=cartIds.split(",");
	  String sql="DELETE FROM cart WHERE "+this.toWhereSql(cart_ids.length);
	  return dao.updateExecuted(sql, new ParamSet(cart_ids));
  }
  /*
   * 根据购物车ID获取商品
   */
  public List<cart> getSelectCart(String cartIds)
  {
	  //将字符串转为数组
	  Object[] cart_ids=cartIds.split(",");
	  String sql="SELECT *FROM cart WHERE "+this.toWhereSql(cart_ids.length);
	  return dao.queryList(sql,new ParamSet(cart_ids) ,ct);
  }
	/*
	 * 查询所有商品
	 */
	private class cartData implements RowData<cart>
	{
         
		@Override
		public cart getRowData(ResultSet rs) throws SQLException {
			cart c=new cart();
			//�����Ը�ֵ
			c.setShoppingname(rs.getString("cart_shoppingname"));
			c.setShoppinginformation(rs.getString("cart_shoppinginformation"));
			c.setShoppingimg(rs.getString("cart_shoppingimg"));
			c.setShoppingscprice(rs.getDouble("cart_shoppingscprice"));
			c.setShoppingsjprice(rs.getDouble("cart_shoppingsjprice"));
			c.setQuantity(rs.getInt("cart_quantity"));
			c.setId(rs.getInt("cart_id"));
		  return c;
		}
		
	}
	/*
	 * 将指定的字符串变成cart_id in('?','?')
	 */
	public String toWhereSql(int len) {
		StringBuilder sb = new StringBuilder("cart_id in(");
		for(int i = 0; i < len; i++) {
			sb.append("?");
			if(i < len - 1) {
				sb.append(",");
			}
		}
		sb.append(")");
		return sb.toString();
	}
}
