package cn.zktr.cartBean;
//订单条目表
public class OrderItem {
   
	 private int orderItem_sid;//商品ID
     private int orderItem_uid;//用户ID
     private String orderItem_sname;//商品名
     private int orderItem_squantity;//商品数量
     private double orderItem_price;//商品价格
     private double orderItem_totalprice;//商品总价
     private String orderItem_image;//商品图片
     private String order_number;//订单号
     //无参构造方法
     public OrderItem() {
 		super();
 		// TODO Auto-generated constructor stub
 	}
    //有参构造方法
     
 	public OrderItem(int orderItem_sid, int orderItem_uid, String orderItem_sname, int orderItem_squantity,
 			double orderItem_price, double orderItem_totalprice, String orderItem_image,
 		    String order_number) {
 		super();
 		this.orderItem_sid = orderItem_sid;
 		this.orderItem_uid = orderItem_uid;
 		this.orderItem_sname = orderItem_sname;
 		this.orderItem_squantity = orderItem_squantity;
 		this.orderItem_price = orderItem_price;
 		this.orderItem_totalprice = orderItem_totalprice;
 		this.orderItem_image = orderItem_image;
 		this.order_number=order_number;
 	}

	public int getOrderItem_sid() {
		return orderItem_sid;
	}

	public void setOrderItem_sid(int orderItem_sid) {
		this.orderItem_sid = orderItem_sid;
	}

	public int getOrderItem_uid() {
		return orderItem_uid;
	}

	public void setOrderItem_uid(int orderItem_uid) {
		this.orderItem_uid = orderItem_uid;
	}

	public String getOrderItem_sname() {
		return orderItem_sname;
	}

	public void setOrderItem_sname(String orderItem_sname) {
		this.orderItem_sname = orderItem_sname;
	}

	public int getOrderItem_squantity() {
		return orderItem_squantity;
	}

	public void setOrderItem_squantity(int orderItem_squantity) {
		this.orderItem_squantity = orderItem_squantity;
	}

	public double getOrderItem_price() {
		return orderItem_price;
	}

	public void setOrderItem_price(double orderItem_price) {
		this.orderItem_price = orderItem_price;
	}

	public double getOrderItem_totalprice() {
		return orderItem_totalprice;
	}

	public void setOrderItem_totalprice(double orderItem_totalprice) {
		this.orderItem_totalprice = orderItem_totalprice;
	}

	public String getOrderItem_image() {
		return orderItem_image;
	}

	public void setOrderItem_image(String orderItem_image) {
		this.orderItem_image = orderItem_image;
	}
	public String getOrder_number() {
		return order_number;
	}

	public void setOrder_number(String order_number) {
		this.order_number = order_number;
	}
}
