package cn.zktr.cartBean;

public class cart {

private int   id;
private	String shoppingname;
private	String shoppinginformation;
private	String shoppingimg;
private	double shoppingscprice;
private	double shoppingsjprice;
private int quantity;
	
	public cart() {
		super();
		// TODO Auto-generated constructor stub
	}
 public double xiaoji()
  {
	 return this.quantity*this.shoppingsjprice;
  }
	public cart(int id,String shoppingname, String shoppinginformation, String shoppingimg, double shoppingscprice,
			double shoppingsjprice,int quantity) {
		super();
		this.id=id;
		this.shoppingname = shoppingname;
		this.shoppinginformation = shoppinginformation;
		this.shoppingimg = shoppingimg;
		this.shoppingscprice = shoppingscprice;
		this.shoppingsjprice = shoppingsjprice;
		this.quantity=quantity;
	}

	public String getShoppingname() {
		return shoppingname;
	}

	public void setShoppingname(String shoppingname) {
		this.shoppingname = shoppingname;
	}

	public String getShoppinginformation() {
		return shoppinginformation;
	}

	public void setShoppinginformation(String shoppinginformation) {
		this.shoppinginformation = shoppinginformation;
	}

	public String getShoppingimg() {
		return shoppingimg;
	}

	public void setShoppingimg(String shoppingimg) {
		this.shoppingimg = shoppingimg;
	}

	public double getShoppingscprice() {
		return shoppingscprice;
	}

	public void setShoppingscprice(double shoppingscprice) {
		this.shoppingscprice = shoppingscprice;
	}

	public double getShoppingsjprice() {
		return shoppingsjprice;
	}

	public void setShoppingsjprice(double shoppingsjprice) {
		this.shoppingsjprice = shoppingsjprice;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
}
