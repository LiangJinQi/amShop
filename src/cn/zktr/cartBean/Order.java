package cn.zktr.cartBean;

public class Order {
  


	private int order_uid;//订单者ID
	private String order_uname;//订单者姓名
	private String order_uTelnumber;
	private String order_address;//订单者地址
	private int order_status;//订单状态
	private String order_time;//下单时间
	private double total;//订单总计
	private String order_number;//订单编号
	private String order_wuliu;//订单物流
    private String order_leaveWords;//商家留言

	public Order() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Order(int order_uid, String order_uname, String order_address, int order_status, String order_time,
			double total,String order_utelnumber,String order_number,String order_wuliu,
			String order_leaveWords) {
		super();
		this.order_uid = order_uid;
		this.order_uname = order_uname;
		this.order_address = order_address;
		this.order_status = order_status;
		this.order_time = order_time;
		this.total = total;
		this.order_uTelnumber=order_utelnumber;
		this.order_number=order_number;
		this.order_wuliu=order_wuliu;
		this.order_leaveWords=order_leaveWords;
	}
	public int getOrder_uid() {
		return order_uid;
	}
	public void setOrder_uid(int order_uid) {
		this.order_uid = order_uid;
	}
	public String getOrder_uname() {
		return order_uname;
	}
	public void setOrder_uname(String order_uname) {
		this.order_uname = order_uname;
	}
	public String getOrder_address() {
		return order_address;
	}
	public void setOrder_address(String order_address) {
		this.order_address = order_address;
	}
	public int getOrder_status() {
		return order_status;
	}
	public void setOrder_status(int order_status) {
		this.order_status = order_status;
	}
	public String getOrder_time() {
		return order_time;
	}
	public void setOrder_time(String order_time) {
		this.order_time = order_time;
	}
	public double getTotal() {
		return total;
	}
	public void setTotal(double total) {
		this.total = total;
	}
	public String getOrder_uTelnumber() {
		return order_uTelnumber;
	}
	public void setOrder_uTelnumber(String order_uTelnumber) {
		this.order_uTelnumber = order_uTelnumber;
	}
	public String getOrder_number() {
		return order_number;
	}
	public void setOrder_number(String order_number) {
		this.order_number = order_number;
	}
	public String getOrderItem_wuliu() {
		return order_wuliu;
	}
	public void setOrder_wuliu(String order_wuliu) {
		this.order_wuliu = order_wuliu;
	}
	public String getOrder_leaveWords() {
		return order_leaveWords;
	}
	public void setOrder_leaveWords(String order_leaveWords) {
		this.order_leaveWords = order_leaveWords;
	}
	
}
