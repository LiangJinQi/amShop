package cn.zktr.cartService;

import cn.zktr.cartBean.Order;
import cn.zktr.cartBean.OrderItem;
import cn.zktr.cartDao.bean.OrderDao;

public class OrderService {
   //创建OrderDao对象
	OrderDao orderDao=new OrderDao();
	//添加订单
	public int addOrder(Order order)
	{
		return orderDao.addOrder(order);
	}
	//添加订单条目
	public int addOrderItem(OrderItem oitem)
	{
		return orderDao.addOrderItem(oitem);
	}
}
