package cn.zktr.cartService;

import java.util.List;

import cn.zktr.cartBean.cart;
import cn.zktr.cartDao.bean.cartdao;

public class cartService {
   cartdao ctd=new cartdao();
	/*
	 * 得到所有购物车信息
	 */
	public List<cart> getCartAll()
	{
		return ctd.getAllcart();
	}
	/*
	 * 更新购物车中的商品数量
	 */
	public int update(int id,int quantity)
	{
		return ctd.update(id, quantity);
	}
	/*
	 * 根据购物车ID删除购物车中的商品
	 */
	public int batchdelete(String cartIds)
	{
		return ctd.batchdelete(cartIds);
	}
	/*
	 * 根据购物车ID获取购物车商品
	 */
	 public List<cart> getSelectCart(String cartIds)
	 {
		 return ctd.getSelectCart(cartIds);
	 }
	
}
