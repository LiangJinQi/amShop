package cn.zktr.cartServlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cn.zktr.cartBean.Order;
import cn.zktr.cartBean.cart;
import cn.zktr.cartService.cartService;

@WebServlet("/CartItemServlet")
public class CartItemServlet extends HttpServlet {
	 cartService cst=new cartService();
	 @Override
	protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		//设置请求编码
		 req.setCharacterEncoding("utf-8");
		//获取方法名
		 String methodName=req.getParameter("method");
		 if(methodName.equals("updateQuantity"))
		 {
			int id = Integer.parseInt(req.getParameter("cart_id"));
			int quantity=Integer.parseInt(req.getParameter("cart_quantity"));
			this.updateQuantity(id, quantity);
			// 给客户端返回一个json对象
			resp.getWriter().print(quantity);
		 }
		 else if(methodName.equals("batchdelete"))
		 {
			 String cartIds=req.getParameter("cart_ids");
			 this.batchdelete(cartIds);
			 resp.sendRedirect("cartServlet");
		 }
		 else if(methodName.equals("submitorder"))
		 {
			 String cartIds=req.getParameter("cart_ids");
			 req.setAttribute("selectCart", this.submitorder(cartIds));
			 req.getRequestDispatcher("jsp/order.jsp").forward(req, resp);
		 }
		 
	}
	 /*
	  * 获取我的购物车
	  */
	 public void mycart()
	 {
		 
	 }
	 /*
	  *修改 购物车中商品数量
	  */
	public void updateQuantity(int id,int quantity)
	{
		cst.update(id, quantity);
	}
	/*
	 * 根据商品ID删除购物车中商品
	 */
   public void batchdelete(String cartIds)
   {
	   cst.batchdelete(cartIds);
   }
   /*
    *结算勾选的商品
    */
   public List<cart> submitorder(String cartIds)
   {
	  return  cst.getSelectCart(cartIds);
   }
}
