package cn.zktr.cartServlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cn.zktr.cartBean.Order;
import cn.zktr.cartBean.OrderItem;
import cn.zktr.cartBean.cart;
import cn.zktr.cartService.OrderService;
import cn.zktr.cartService.cartService;
import cn.zktr.util.utilUUID;
@WebServlet("/OrderServlet")
public class OrderServlet extends HttpServlet {
     //创建对象
	OrderService ose=new OrderService();
	cartService cse=new cartService();
	@Override
	protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		//请求编码
		req.setCharacterEncoding("utf-8");
		//获取方法名
		String methodName=req.getParameter("method");
		if(methodName.equals("addOrder"))
		{
			String shXxArray=req.getParameter("shXxArray")+",";
			String OrderIds=req.getParameter("OrderIds");
			int index=0;
			//创建数组
			List<String> list=new ArrayList<String>();
		    while((index=shXxArray.indexOf(","))!=-1)
		    {
		        list.add(shXxArray.substring(0, index));
		    	shXxArray=shXxArray.substring(index+1);
		    }
		  //创建Order对象
		    Order order=new Order();
		    order.setOrder_uid(1);
		    order.setOrder_uname(list.get(0));
		    order.setOrder_address(list.get(1));
		    order.setOrder_uTelnumber(list.get(2));
		    order.setTotal(Double.parseDouble(list.get(3)));
		    order.setOrder_status(1);
		    order.setOrder_time(String.format("%tF %<tT", new Date()));
		    //利用UUID工具产生32为的订单编号
		    String orderNumber=utilUUID.uuid();
		    order.setOrder_number(orderNumber);
		     //选择物流
            order.setOrder_wuliu("中通快递");
            //给商品留言
            order.setOrder_leaveWords("哈哈，好评，能不能返现五元？");
		    //调用方法addOrder,生成订单
		    addOrder(order);
		    //创建OrderItem对象
		    OrderItem oitem=new OrderItem();
		    //获取选择的商品生成订单
		    List<cart> cart=cse.getSelectCart(OrderIds);
	         for(cart c: cart)
	         {
	            oitem.setOrderItem_uid(1);
	            oitem.setOrderItem_sid(c.getId());
	            oitem.setOrderItem_sname(c.getShoppingname());
	            oitem.setOrderItem_squantity(c.getQuantity());
	            oitem.setOrderItem_price(c.getShoppingsjprice());
	            oitem.setOrderItem_totalprice((c.getShoppingsjprice()*c.getQuantity()));
	            oitem.setOrderItem_image(c.getShoppingimg());
	            oitem.setOrder_number(orderNumber);
	            //调用方法addItem方法，生成订单
	            addOrderItem(oitem);
	         }      
		}
	}
	/*
	 * 添加订单addOrder
	 */
	   public void addOrder(Order order)
	   {
		   ose.addOrder(order);
	   }
	//添加订单条目addOrderItem
	   public void  addOrderItem(OrderItem orderItem)
	   {
		   ose.addOrderItem(orderItem);
	   }
}
