package cn.zktr.cartServlet;
import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cn.zktr.cartBean.cart;
import cn.zktr.cartService.cartService;
@WebServlet("/cartServlet")
public class cartServlet extends HttpServlet {
   cartService cst=new cartService();
   @Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
	   //请求设置编码
		req.setCharacterEncoding("utf-8");
		//创建cart的list集合
     	List<cart> cart=cst.getCartAll();
    	//将cart保存到req中
     	req.setAttribute("cart", cart);
    	//转发
    	req.getRequestDispatcher("jsp/mycart.jsp").forward(req, resp);
	}
}